import { ITheme } from "@app/lib/styled";

export const theme: ITheme = {
  colors: {
    dark: [
      "rgba(0, 0, 0, 1)",
      "rgba(0, 0, 0, .875)",
      "rgba(0, 0, 0, .625)",
      "rgba(0, 0, 0, .5)",
      "rgba(0, 0, 0, .375)",
      "rgba(0, 0, 0, .25)"
    ],
    light: [
      "rgba(255, 255, 255, 1)",
      "rgba(255, 255, 255, .875)",
      "rgba(255, 255, 255, .625)",
      "rgba(255, 255, 255, .5)",
      "rgba(255, 255, 255, .375)",
      "rgba(255, 255, 255, .25)"
    ]
  }
};

export default theme;
