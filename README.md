# analogic.al

[![Build Status](https://travis-ci.com/ricardocasares/analogical.svg?branch=master)](https://travis-ci.com/ricardocasares/analogical)
[![codecov](https://codecov.io/gh/ricardocasares/analogical/branch/master/graph/badge.svg)](https://codecov.io/gh/ricardocasares/analogical)

This is my [personal site](https://analogic.al)

## Stack

- React
- Next
- Emotion
- TypeScript
- Docker
- Now.sh
